# OD华为机试

#### 介绍
华为OD-java工程师机试练习

#### 练习地址
https://www.nowcoder.com/exam/oj/ta?tpId=37
#### 知识要点
HJ1字符串最后一个单词的长度

    001.从键盘录入Scanner：Scanner input = new Scanner(System.in)
    002.循环录入：input.hasNext()
    003.字符串截取：str = str.substring(str.lastIndexOf(" ") + 1)
HJ2计算某字符出现次数

    001.字符串大小写比较：toLowerCase、equalsIgnoreCase
HJ3明明的随机数
    
    001.字符数字去重：Set set = new HashSet();利用Set集合无序不可重复的特性进行元素过滤
                    Set<Object> haoma = new LinkedHashSet<Object>();链表的哈希集合：有顺序，不重复。
    002.数组排序：冒泡排序、Arrays.sort([一个数组])
HJ4字符串分隔

    001.字符串的拼接与截取：string=string.substring(8)
    002.不定次数循环：while
HJ5进制转换 

    001.16进制转10进制：Integer.decode(str)或 Long.parseLong(str.substring(2),16)
    002.10进制转16进制：Long.toHexString(n).toUpperCase()

HJ6质数因子
   
    001. 数的平方根： Math.sqrt(num)
    002. 流录入效率更高：BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
  
HJ7取近似值

    001.Math方法的应用： 四舍五入Math.round(dbNum)、向上取整Math.ceil(dbNum)、向下取整Math.floor(dbNum)
HJ8合并表记录

    001.唯一且有序的键值对： TreeMap<Integer,Integer> map=new TreeMap<>();
    002.set的遍历：（Map的遍历）
        Set<Integer> keySet=map.keySet();
        Iterator<Integer> iterator =keySet.iterator();
        while(iterator.hasNext()){
            int key = iterator.next();
            System.out.println(key + " " + map.get(key));
        }
HJ9提取不重复的整数
 
    001.逆序不重复 for循环逆序，map去重   
HJ10字符个数统计 
    
    001.字符串去重求长度Set集合无序不重复  
HJ11数字颠倒、HJ12字符串反转、HJ13句子逆序
 
    001.字符串、整数逆序：反向遍历
HJ14字符串排序

    001.字符串字典顺序排序Arrays.sort()或冒泡排序+compareTo
HJ15求int型正整数在内存中存储时1的个数
    
    001.进制转换
    十进制转成十六进制：Integer.toHexString(int i)
    
    十进制转成八进制：Integer.toOctalString(int i)
    
    十进制转成二进制：Integer.toBinaryString(int i)
    
    十六进制转成十进制：Integer.valueOf("FFFF",16).toString()
    
    八进制转成十进制：Integer.valueOf("876",8).toString()
    
    二进制转十进制：Integer.valueOf("0101",2).toString()
HJ16购物单
HJ17坐标移动

    001.字符串切割string.split(";");
    002.字符整数计算int t=str.charAt(i)-'0';
HJ18识别有效的IP地址和掩码并进行分类统计
HJ19简单错误记录

    001.LinkedHashMap可以去重
    002.getOrDefault(key,0)+1 key去重，value加1
HJ20密码验证合格程序

    001.不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）
        即（i=i+3,i+1=i+4,i+2=i+5）    
    
