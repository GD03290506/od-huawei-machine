package practice_case;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
* 描述
密码要求:

1.长度超过8位

2.包括大小写字母.数字.其它符号,以上四种至少三种

3.不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）

数据范围：输入的字符串长度满足 1 \le n \le 100 \1≤n≤100
输入描述：
一组字符串。

输出描述：
如果符合要求输出：OK，否则输出NG

示例1
输入：
021Abc9000
021Abc9Abc1
021ABC9000
021$bc9000
复制
输出：
OK
NG
NG
OK
* */
public class HJ20密码验证合格程序 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        String string;
        while ((string=br.readLine())!=null){
            int count =0;
            char[] c = string.toCharArray();
            Boolean[] booleans = new Boolean[4];
            //长度小于8淘汰
            if (string.length()<8){
                sb.append("NG").append("\n");
                continue;
            }
            //包括大小写字母.数字.其它符号,以上四种至少三种
            for (int i=0;i<c.length;i++){
                if('A'<=c[i]&&c[i]<='Z'){
                    booleans[0]=true;
                }else if('a'<=c[i]&&c[i]<='z'){
                    booleans[1]=true;
                }else if ('0'<=c[i]&&c[i]<='9'){
                    booleans[2]=true;
                }else {//其他符号
                    booleans[3]=true;
                }
            }
            for (Boolean b : booleans){
                if(b!=null&&b==true){
                    count++;
                }
            }
            //不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）
            //（i=i+3,i+1=i+4,i+2=i+5）
            for (int i = 0; i < c.length - 5; i++) {
                for (int j = i + 3; j < c.length - 2; j++) {
                    if (c[i] == c[j] && c[i + 1] == c[j + 1] && c[i + 2] == c[j + 2]) {
                        count = 0;
                    }
                }
            }
            if(count>=3){
                sb.append("OK").append("\n");
            }else {
                sb.append("NG").append("\n");
            }
        }
        System.out.println(sb);
    }
}
