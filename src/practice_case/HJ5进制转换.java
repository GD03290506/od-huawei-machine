package practice_case;

import java.util.Scanner;
/*
* 描述
写出一个程序，接受一个十六进制的数，输出该数值的十进制表示。

数据范围：保证结果在  1≤n≤2
31
 −1
输入描述：
输入一个十六进制的数值字符串。

输出描述：
输出该数值的十进制字符串。不同组的测试用例用\n隔开。

示例1
输入：
0xAA
输出：
170
*
* */
public class HJ5进制转换 {
    /*public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNext()){
            String str = input.nextLine();
            System.out.println(Integer.decode(str));
        }
    }*/
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNext()){
            long n = input.nextLong();
            System.out.println(Long.toHexString(n).toUpperCase());

        }
    }
}
