package practice_case;

import java.util.Scanner;

/*
* 描述
输入一个 int 型的正整数，计算出该 int 型数据在内存中存储时 1 的个数。

数据范围：保证在 32 位整型数字范围内
输入描述：
 输入一个整数（int类型）

输出描述：
 这个数转换成2进制后，输出1的个数

示例1
输入：
5
复制
输出：
2
复制
示例2
输入：
0
复制
输出：
0
* */
public class HJ15求int型正整数在内存中存储时1的个数 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(input.hasNext()){
            int num=input.nextInt();
            String string = Integer.toBinaryString(num);
            string=string.replace("0","");
            System.out.println(string.length());
        }
    }
}
