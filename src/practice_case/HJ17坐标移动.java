package practice_case;

import java.util.Scanner;

/*
*描述
开发一个坐标计算工具， A表示向左移动，D表示向右移动，W表示向上移动，S表示向下移动。从（0,0）点开始移动，从输入字符串里面读取一些坐标，并将最终输入结果输出到输出文件里面。

输入：

合法坐标为A(或者D或者W或者S) + 数字（两位以内）

坐标之间以;分隔。

非法坐标点需要进行丢弃。如AA10;  A1A;  $%$;  YAD; 等。

下面是一个简单的例子 如：

A10;S20;W10;D30;X;A1A;B10A11;;A10;

处理过程：

起点（0,0）

+   A10   =  （-10,0）

+   S20   =  (-10,-20)

+   W10  =  (-10,-10)

+   D30  =  (20,-10)

+   x    =  无效

+   A1A   =  无效

+   B10A11   =  无效

+  一个空 不影响

+   A10  =  (10,-10)

结果 （10， -10）

数据范围：每组输入的字符串长度满足 1\le n \le 10000 \1≤n≤10000  ，坐标保证满足 -2^{31} \le x,y \le 2^{31}-1 \−2
31
 ≤x,y≤2
31
 −1  ，且数字部分仅含正数
输入描述：
一行字符串

输出描述：
最终坐标，以逗号分隔

示例1
输入：
A10;S20;W10;D30;X;A1A;B10A11;;A10;
复制
输出：
10,-10
复制
示例2
输入：
ABC;AKL;DA1;
复制
输出：
0,0
* */
public class HJ17坐标移动 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(input.hasNext()){
            int x=0;
            int y=0;
            String string = input.nextLine();
            String[] strings = string.split(";");
            for (String str :strings){
                int v=0;
                if ("".equals(str) ||str.length()>3) continue;
                for (int i=1;i<str.length();i++){//字符串的第一个字符不会循环
                    int t=str.charAt(i)-'0';
                    if(t>=0&&t<=9){
                        if(i==1&&str.length()!=2){
                            v=v+t*10;//加十位
                        }else{
                            v=v+t;//加个位
                        }
                    }else {
                        //排除A1A
                        v=0;
                        break;
                    }
                }
                char c =str.charAt(0);//读取上下左右字母
                switch (c){
                    case 'A':
                        x=x-v;
                        break;
                    case 'D':
                        x=x+v;
                        break;
                    case 'W':
                        y=y+v;
                        break;
                    case 'S':
                        y=y-v;
                        break;
                }
            }
            System.out.println(x+","+y);
        }
    }
}
