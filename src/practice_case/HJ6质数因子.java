package practice_case;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
*描述
功能:输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）（如180的质因子为2 2 3 3 5 ）


数据范围： 1 \le n \le 2 \times 10^{9} + 14 \1≤n≤2×10
9
 +14
输入描述：
输入一个整数

输出描述：
按照从小到大的顺序输出它的所有质数的因子，以空格隔开。

示例1
输入：
180

输出：
2 2 3 3 5
* */
public class HJ6质数因子 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = "";
        while ((str = br.readLine()) != null) {
            int num = Integer.parseInt(str);
            StringBuffer sb = new StringBuffer();
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    sb.append(i).append(" ");
                    num = num / i;
                    i--;
                }
            }
            sb.append(num + " ");
            System.out.println(sb.toString());
        }
    }
}
