package practice_case;

import java.util.*;

/*
* 描述
输入一个 int 型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
保证输入的整数最后一位不是 0 。

数据范围： 1 \le n \le 10^{8} \1≤n≤10
8

输入描述：
输入一个int型整数

输出描述：
按照从右向左的阅读顺序，返回一个不含重复数字的新的整数

示例1
输入：
9876673
复制
输出：
37689
*
* */
public class HJ9提取不重复的整数 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNext()){
            String num = input.nextLine();
            char[] chars = num.toCharArray();
            Map map =new HashMap();
            int sum = 0;
            for(int i=chars.length-1;i>=0;i--){
                sum = map.size();
                map.put(chars[i],chars[i]);
                if(sum==map.size()){
                    continue;
                }
                System.out.print(chars[i]);
            }
        }

    }
}
