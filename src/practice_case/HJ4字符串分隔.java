package practice_case;

import java.util.Scanner;

/*
* 描述
•输入一个字符串，请按长度为8拆分每个输入字符串并进行输出；

•长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
输入描述：
连续输入字符串(每个字符串长度小于等于100)

输出描述：
依次输出所有分割后的长度为8的新字符串

示例1
输入：
abc
输出：
abc00000
*
* */
public class HJ4字符串分隔 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNext()){
            String string = input.nextLine();
            sub(string);
        }
    }
    public static void sub(String string){
        while (string.length()>=8){
            System.out.println(string.substring(0,8));
            string=string.substring(8);
        }
        if(string.length()<8 && string.length()>0){
            string = string+"00000000";
            System.out.println(string.substring(0,8));
        }

    }
}
